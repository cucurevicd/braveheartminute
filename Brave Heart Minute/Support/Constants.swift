//
//  Config.swift
//  EKapija
//
//  Created by Intellex on 12/16/15.
//  Copyright © 2015 Intellex. All rights reserved.
//

import Foundation
import UIKit

//Size constants

let MAIN_SCREEN = UIScreen.main
let SCREEN_WIDTH = UIScreen.main.bounds.width
let SCREEN_HEIGHT = UIScreen.main.bounds.height

// App constants
let DELEGATE = UIApplication.shared.delegate as! AppDelegate

// Language constants
let ERROR = NSLocalizedString("Error", comment: "error")
let NOT_IMPLEMENTED = NSLocalizedString("Not implemented", comment: "Not implemented")
let NO_INTERNET_CONNECTION = NSLocalizedString("No network connection!", comment: "No network connection!")
let SUCCESS = NSLocalizedString("Successful", comment: "Successful")
let OK = NSLocalizedString("OK", comment: "ok")
let YES = NSLocalizedString("YES", comment: "yes")
let NO = NSLocalizedString("NO", comment: "no")
let CANCEL = NSLocalizedString("Cancel", comment: "cancel")
let ALERT = NSLocalizedString("Alert", comment: "alert message title")
let EMPTY_STRING = ""
let APP_NAME = Bundle.main.infoDictionary?[kCFBundleNameKey as String] as! String
let APP_ID = Bundle.main.infoDictionary?[kCFBundleIdentifierKey as String] as! String

// Client constants
let CLIENT_TYPE = "ios"
let CLIENT_LOCALE = String(describing: Bundle.main.preferredLocalizations.first ?? "")

// NSUser defaults const
let DEVICE_TOKEN = "device_token"
let AUTH_TOKEN = "auth_token"
let FIREBASE_TOKEN = "firebase_token"
