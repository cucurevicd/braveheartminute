//
//  ValidationUtil.swift
//  Brave Heart Minute
//
//  Created by Vladimir Djokanovic on 8/21/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit

class ValidationUtil: NSObject {

    // MARK:- Validation
    
    /**
     Validate is format of email correct
     
     - parameter testStr: Email
     
     - returns: Bool value
     */
    static func isValidEmail(_ testStr: String) -> Bool {
        
        let emailRegEx = "[A-Z0-9a-z._%+-]+@[A-Za-z0-9.-]+\\.[A-Za-z]{2,}"
        
        let emailTest = NSPredicate(format: "SELF MATCHES %@", emailRegEx)
        return emailTest.evaluate(with: testStr)
    }
    
    /**
     Validate is format of phone number valid
     
     - parameter phone: The phone number
     
     - returns: Bool value
     */
    static func isPhoneValid(_ phone: String) -> Bool {
        
        let PHONE_REGEX = "\\+(9[976]\\d|8[987530]\\d|6[987]\\d|5[90]\\d|42\\d|3[875]\\d|2[98654321]\\d|9[8543210]|8[6421]|6[6543210]|5[87654321]|4[987654310]|3[9643210]|2[70]|7|1)\\d{1,14}"
        let phoneTest = NSPredicate(format: "SELF MATCHES %@", PHONE_REGEX)
        let result = phoneTest.evaluate(with: phone)
        return result
    }
    
    /**
     Check is input field not empty
     
     - parameter inputField: String of input field
     
     - returns: Bool value
     */
    static func isInputFiledEmpty(_ inputField: String?) -> Bool {
        
        guard inputField != nil else {
            return true
        }
        if inputField?.characters.count == 0 {
            
            return true
        }
        else {
            
            return false
        }
    }
    
    /**
     Check if credit card number is valid
     
     - parameter cardNumber: card number to be validated
     
     - returns: Bool value
     */
    static func isCreditCardNumberValid(_ cardNumber: String) -> Bool {
        
        let CREDIT_CARD_REGEX = "^[0-9]{14,16}$"
        let creditCardNumberTest = NSPredicate(format: "SELF MATCHES %@", CREDIT_CARD_REGEX)
        let result = creditCardNumberTest.evaluate(with: cardNumber)
        return result
    }
    
    /**
     Check if security code is valid
     
     - parameter securityCode: security code to be validated
     
     - returns: Bool value
     */
    static func isSecurityCodeValid(_ securityCode: String) -> Bool {
        
        let SECURITY_CODE_REGEX = "^[0-9]{3,4}$"
        let securityCodeTest = NSPredicate(format: "SELF MATCHES %@", SECURITY_CODE_REGEX)
        let result = securityCodeTest.evaluate(with: securityCode)
        return result
    }
    
    /**
     Check if IBAN is valid
     
     - parameter iban: iban to be checked
     
     - returns: Bool value
     */
    static func isIBANValid(_ iban: String) -> Bool {
        
        let IBAN_REGEX = "^[A-Z]{2}[A-Z0-9]{0,28}$"
        let ibanTest = NSPredicate(format: "SELF MATCHES %@", IBAN_REGEX)
        let result = ibanTest.evaluate(with: iban)
        return result
    }
    
    /**
     Check if BIC is valid
     
     - parameter bic: bic number to be validated
     
     - returns: Bool value
     */
    static func isBICValid(_ bic: String) -> Bool {
        
        let BIC_REGEX = "^([a-zA-Z]{4}[a-zA-Z]{2}[a-zA-Z0-9]{2}([a-zA-Z0-9]{3})?)?$"
        let bicTest = NSPredicate(format: "SELF MATCHES %@", BIC_REGEX)
        let result = bicTest.evaluate(with: bic)
        return result
    }
    
    /**
     Check if amount is valid
     
     - parameter amount: amount number to be validated
     
     - returns: Bool value
     */
    static func isAmountValid(_ amount: String) -> Bool {
        
        let AMOUNT_REGEX = "^[^0][0-9]+([,.][0-9]{1,4})?$"
        let amountTest = NSPredicate(format: "SELF MATCHES %@", AMOUNT_REGEX)
        let result = amountTest.evaluate(with: amount)
        return result
    }
    
    /**
     Check if any text field is empty
     
     - parameter textFieldsArray: Array of text fields to be checked
     
     - returns: Bool value
     */
    static func isAnyTextFieldEmpty(_ textFieldsArray: [UITextField]) -> Bool {
        
        for tf in textFieldsArray {
            if ValidationUtil.isInputFiledEmpty(tf.text!) {
                
                Util.alertWithButtonPresed(ERROR, message: "\(tf.placeholder!) is mandatory", buttonTitle: "Try again", okButtonPressed: { (pressed) in
                    
                    tf.becomeFirstResponder()
                })
                return true
            }
        }
        return false
        
    }
    
    static func isPromoCodeValid(_ promocode: String) -> Bool {
        
        let PROMOCODE_REGEX = "[A-Za-z0-9]{3}-[A-Za-z0-9]{4}-[A-Za-z0-9]{4}"
        let promoCodeTest = NSPredicate(format: "SELF MATCHES %@", PROMOCODE_REGEX)
        let result = promoCodeTest.evaluate(with: promocode)
        return result
        
    }
    
    /**
     Are passwords same
     
     - parameter password:        password
     - parameter confirmPassword: confirm password
     
     - returns: return true if passwords are equal
     */
    class func isPasswordSame(_ password: String, confirmPassword: String) -> Bool {
        if password == confirmPassword {
            return true
        }
        else {
            return false
        }
    }
    
    
    /// Is password length greater then 8 characters
    ///
    /// - Parameter password: Password
    /// - Returns: bool
    class func isPasswordLengthMinimum(password: String, lenght: Int) -> Bool {
        
        return password.characters.count >= lenght ? true : false
        
    }

}
