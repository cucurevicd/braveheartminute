//
//  Util.swift
//  DeltaCity
//
//  Created by Intellex on 9/21/15.
//  Copyright (c) 2015 Intellex. All rights reserved.
//

import UIKit
import Foundation
import MapKit
import SafariServices

/**
Social media types enum

- facebook:   URL open facebook profile
- twitter:    URL open twitter profile
- googlePlus: URL open google profile
- linkedin:   URL open linkedin profile
*/
enum SocialMediaType: String {
	
	case facebook = "fb://profile/", twitter = "twitter://user?screen_name=", googlePlus = "gplus://plus.google.com/", linkedin = "linkedin://profile?id="
}

/// Completion handlers
typealias FinishHandler = (Void) -> Void
typealias SuccessHandler = (_ success: Bool) -> Void
typealias ButtonPressed = (Void) -> Void
typealias TextFieldButtomPressed = (_ inputText: String) -> Void

class Util: NSObject {
	
	// MARK:- Image with colors
	
	/**
	Create gradient color layer in view as background

	- parameter view:        View with gradient color
	- parameter topColor:    Top color
	- parameter bottomColor: Bottom color
	*/
	static func layerGradient(_ view: UIView, topColor: UIColor, bottomColor: UIColor) {
		
		view.backgroundColor = UIColor.clear
		let layer: CAGradientLayer = CAGradientLayer()
		layer.frame.size = view.frame.size
		layer.frame.origin = CGPoint(x: 0.0, y: 0.0)
		
		layer.colors = [topColor.cgColor, bottomColor.cgColor]
		view.layer.insertSublayer(layer, at: 0)
	}
	
	/**
	Create image with gradient colors

	- parameter image:       Image with gradient colors
	- parameter topColor:    Top color
	- parameter bottomColor: Bottom color

	- returns: Image with gradient colors
	*/
	static func gradientImage(_ image: UIView, topColor: UIColor, bottomColor: UIColor) -> UIImage {
		
		UIGraphicsBeginImageContext(image.bounds.size)
		let context = UIGraphicsGetCurrentContext()
		
		let currentImage = UIImage()
		currentImage.draw(at: CGPoint(x: 0, y: 0))
		let colorSpace = CGColorSpaceCreateDeviceRGB()
		let locations: [CGFloat] = [0.0, 1.0]
		let bottom = bottomColor.cgColor
		let top = topColor.cgColor

		let colors = [top, bottom] as NSArray
		let gradient = CGGradient(colorsSpace: colorSpace,
			colors: colors, locations: locations)
		
		let startPoint = CGPoint(x: image.bounds.size.width / 2, y: image.bounds.size.height / 2)
		let endPoint = CGPoint(x: image.bounds.size.width / 2, y: image.bounds.size.height)
		context?.drawLinearGradient(gradient!, start: startPoint, end: endPoint, options: .drawsBeforeStartLocation)
		
		let finalImage = UIGraphicsGetImageFromCurrentImageContext()
		UIGraphicsEndImageContext()
		
		return finalImage!
	}
	
	// Shadows
	
	/**
	Add shadow under view

	- parameter view: View where shadow will be applied
	*/
	static func addDownShadow(_ view: UIView) {
		
		view.layer.shadowColor = UIColor.darkGray.cgColor
		view.layer.shadowOpacity = 1
		view.layer.shadowOffset = CGSize.zero
		view.layer.shadowRadius = 2
	}
	
	/**
	Add shadow on right

	- parameter view: View where shadow will be applied
	*/
	static func addRightShadow(_ view: UIView) {
		
		view.layer.shadowColor = UIColor.black.cgColor
		view.layer.shadowOpacity = 1
		view.layer.shadowOffset = CGSize(width: -1, height: 0)
		view.layer.shadowRadius = 4
		
	}
    
    
    /// Convert Hex color to UIColor
    ///
    /// - Parameter hex: e.g. #d3d3d3
    /// - Returns: UIColor
    static func hexStringToUIColor (hex:String) -> UIColor {
        var cString:String = hex.trimmingCharacters(in: .whitespacesAndNewlines).uppercased()
        
        if (cString.hasPrefix("#")) {
            cString.remove(at: cString.startIndex)
        }
        
        if ((cString.characters.count) != 6) {
            return UIColor.gray
        }
        
        var rgbValue:UInt32 = 0
        Scanner(string: cString).scanHexInt32(&rgbValue)
        
        return UIColor(
            red: CGFloat((rgbValue & 0xFF0000) >> 16) / 255.0,
            green: CGFloat((rgbValue & 0x00FF00) >> 8) / 255.0,
            blue: CGFloat(rgbValue & 0x0000FF) / 255.0,
            alpha: CGFloat(1.0)
        )
    }
	
    
	// MARK:- Date formater
	
	/**
	Create date from string with provided format

	- parameter dateString: The date string
	- parameter dateFormat: The date format

	- returns: The date or nil
	*/
	static func dateFormString(_ dateString: String?, dateFormatString: String) -> Date? {

		if dateString != nil{
			let dateFormater = DateFormatter()
			dateFormater.dateFormat = dateFormatString

			if let date = dateFormater.date(from: dateString!) {
                
                print("date \(date)")
				return date

			}
		}

		return nil
	}
	
	/**
	Create string from date

	- parameter date:   NSDate which will be transformed into string
	- parameter format: Format of date in string value

	- returns: String represent of date
	*/
	static func stringFromDate(_ date: Date?, format: String) -> String {
		
		if date != nil {
			
			let dateFormatter = DateFormatter()
			dateFormatter.dateFormat = format
			dateFormatter.timeZone = TimeZone.autoupdatingCurrent
			return dateFormatter.string(from: date!)
		}
            
		else {
			return EMPTY_STRING
		}
	}
	
	/**
     Get string from date in locale format
     
     - parameter date: The date for formating
     
     - returns: Formated string from date
     */
	static func stringFromDateLocalFormat(_ date: Date) -> String {

		let dateFormatter = DateFormatter()
		let format = DateFormatter.dateFormat(fromTemplate: "dd MM YYYY", options: 0, locale: Locale.current)
		dateFormatter.dateFormat = format
		return dateFormatter.string(from: date)
	}

	
	/**
	Create date string in local time zone

	- parameter dateString:   String represent of date
	- parameter inputFormat:  Input string format
	- parameter outputFormat: Output string format

	- returns: String represent of date
	*/
	static func stringFromDateString(_ dateString: String, inputFormat: String, outputFormat: String) -> String {
		
		let dateFormatter = DateFormatter()
		dateFormatter.dateFormat = inputFormat
		dateFormatter.timeZone = TimeZone.autoupdatingCurrent
		let date = dateFormatter.date(from: dateString)
		
		let stringDate = self.stringFromDate(date!, format: outputFormat)
		return stringDate
	}
    
    /// Get component value
    ///
    /// - Parameters:
    ///   - component: Month, Dat, Seconds, etc.
    ///   - date: Date from which wanto to found component value
    /// - Returns: Value of date component
    static func getComponentFromDate(component: Calendar.Component, _ date:Date) -> Int {
        
        let calendar = Calendar.current
        let currentComponent = calendar.component(component, from: date)
        
        return currentComponent
    }
    
    // MARK:- Minute,Day, Week ago from date
    
    /**
     Get string representation of how many time is passed from date
     
     - parameter date: Date from which passed time is count
     
     - returns: String representation of passed time
     */
    static func timePassedFrom(_ date: Date) -> String {
        
        // Get minutes passed between 2 dates with time zone correction
        let minutesBetweenDates = Int(Date().timeIntervalSince(date) / 60)
        
        // Get difference in String format
        var interval = ""
        
        // Interval lesser than 1,
        if minutesBetweenDates <= 1 {
            interval = "just now"
            
            // Interval is lesser than 1h
        } else if minutesBetweenDates <= 60 {
            interval = String(format: "%d minute%@ ago", minutesBetweenDates, minutesBetweenDates == 1 ? "" : "s")
            
            // Interval lesser than 1d
        } else if minutesBetweenDates <= 60 * 24 {
            interval = String(format: "%d hour%@ ago", minutesBetweenDates / 60, minutesBetweenDates / 60 == 1 ? "" : "s")
            
            // Interval lesser than 1w
        } else if minutesBetweenDates <= 60 * 24 * 7 {
            interval = String(format: "%d day%@ ago", minutesBetweenDates / 1440, minutesBetweenDates / 1440 == 1 ? "" : "s")
            
            // Interval lesser than 1M
        } else if minutesBetweenDates <= 60 * 24 * 7 * 4 {
            interval = String(format: "%d week%@ ago", minutesBetweenDates / 10080, minutesBetweenDates / 10080 == 1 ? "" : "s")
            
            // Interval lesser than 1Y
        } else if minutesBetweenDates <= 60 * 24 * 7 * 4 * 12 {
            interval = String(format: "%d month%@ ago", minutesBetweenDates / 40320, minutesBetweenDates / 40320 == 1 ? "" : "s")
            
            // Interval is more than a year
        } else {
            interval = String(format: "%d year%@ ago", minutesBetweenDates / 483840, minutesBetweenDates / 483840 == 1 ? "" : "s")
        }
        
        // Return interval
        return interval;
    }
    
    //MARK:- Location
    
    
    /// Set country code iso for country name
    ///
    /// - Parameter countryName: country full name
    /// - Returns: iso name
    static func countryCode(_ countryName: String?) -> String?{
        
        if countryName == nil{
            
            return nil
        }
        
        for code in Locale.isoRegionCodes as [String] {
            let id = Locale.identifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
            let name = (Locale(identifier: "en_UK") as NSLocale).displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Country not found for code: \(code)"
            
            if countryName == name{
                
                return code
            }
        }
        
        return nil
    }
	
	
	
	// MARK:- Alert
	
	/**
	Alert with title. One button is predefined ("OK")

	- parameter title:   Title message of alert
	- parameter message: Message of alert
	*/
	static func alertWithTitle(_ title: String, message: String) {
		
		let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: "OK", style: .default, handler: { action in
			switch action.style {
			case .default:
				print("default")
				_ = UIApplication.topViewController().popoverPresentationController
				
			case .cancel:
				print("cancel")
				
			case .destructive:
				print("destructive")
			}
			}))
		
		DispatchQueue.main.async(execute: {
			UIApplication.topViewController().present(alert, animated: true, completion: nil)
		})
	}
	
	/**
	Create tost with alert message. Tost will be removed after set duration

	- parameter title:    Title of alert tost
	- parameter message:  Message of alert tost
	- parameter duration: Duration of tost
	*/
	static func alertTost(_ title: String, message: String, duration: Double) {
		
		let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
		
		DispatchQueue.main.async(execute: {
			UIApplication.topViewController().present(alert, animated: true, completion: nil)
		})
		
		let delayTime = DispatchTime.now() + Double(Int64(duration * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
		DispatchQueue.main.asyncAfter(deadline: delayTime) {
			alert.dismiss(animated: true, completion: nil)
		}
	}
	
	/**
	Create alert with completion handler when button ok is pressed

	- parameter title:           Alert title
	- parameter message:         Alert message
	- parameter okButtonPressed: Block to be execute when button is pressed
	*/
	static func alertWithButtonPresed(_ title: String, message: String, buttonTitle: String?, okButtonPressed: @escaping ButtonPressed) {
		
		let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: buttonTitle != nil ? buttonTitle : "OK", style: .default, handler: { action in
			
			switch action.style {
				
			case .default:
				print("default")
				_ = UIApplication.topViewController().popoverPresentationController
				okButtonPressed()
				
			case .cancel:
				print("cancel")
				
			case .destructive:
				print("destructive")
			}
			}))
		
		DispatchQueue.main.async(execute: {
			UIApplication.topViewController().present(alert, animated: true, completion: nil)
		})
	}


	/// Alert view with text input field
	///
	/// - parameter title:                Name of alert
	/// - parameter message:              Message
	/// - parameter buttonTitle:          Ok button title. Other button is cancel
	/// - parameter textFieldPlaceholder: TextField placeholder
	/// - parameter okButtonPressed:      Return value from textfield
	static func alertWithTextField(_ title: String, message: String, buttonTitle: String, textFieldPlaceholder: String, okButtonPressed: @escaping TextFieldButtomPressed){

		let alert = UIAlertController(title: title, message: message,preferredStyle: .alert)

		alert.addTextField { (textField) in
			textField.placeholder = textFieldPlaceholder
		}

		alert.addAction(UIAlertAction(title: CANCEL, style: .cancel, handler: { (_) in
			
			_ = UIApplication.topViewController().popoverPresentationController
		}))

		alert.addAction(UIAlertAction(title: NSLocalizedString("OK", comment: ""), style: .default, handler: { (_) in
			let input = alert.textFields![0]

			if input.text != EMPTY_STRING{
				okButtonPressed(input.text!)
			}
		}))

		DispatchQueue.main.async(execute: {
			UIApplication.topViewController().present(alert, animated: true, completion: nil)
		})
	}

	/**
	Create alert with two option.

	- parameter title:        The title of alert
	- parameter message:      The message of alert
	- parameter closeTitle:   Close button tile
	- parameter actionTitle:  Action button tile
	- parameter closeButton:  Close callback
	- parameter actionButton: Action callback
	*/
	static func alertWithTwoOptions(_ title: String, message: String, closeTitle: String, actionTitle: String, closeButton: @escaping (ButtonPressed), actionButton: @escaping (ButtonPressed)) {
		
		let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
		alert.addAction(UIAlertAction(title: closeTitle, style: .default, handler: { action in
			
			closeButton()
			}))
		
		alert.addAction(UIAlertAction(title: actionTitle, style: .default, handler: { action in
			actionButton()
			}))
		
		DispatchQueue.main.async(execute: {
			UIApplication.topViewController().present(alert, animated: true, completion: nil)
		})
	}
	
	/**
	Create tost with completion block after duration.

	- parameter title:    Title of alert
	- parameter message:  MEssage text
	- parameter duration: Duration of tost
	- parameter finish:   Block to be execute after duration
	*/
	static func alertTostWithCompletionHandler(_ title: String, message: String, duration: Double, finish: @escaping SuccessHandler) {
		
		let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
		
		DispatchQueue.main.async(execute: {
			UIApplication.topViewController().present(alert, animated: true, completion: nil)
		})
		
		let delayTime = DispatchTime.now() + Double(Int64(duration * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
		DispatchQueue.main.asyncAfter(deadline: delayTime) {
			alert.dismiss(animated: true, completion: nil)
			finish(true)
		}
	}
	
	/**
	Alert view with "Not implemented" message
	*/
	static func notImplemented() {
		
		let alert = UIAlertController(title: NOT_IMPLEMENTED, message: nil, preferredStyle: .alert)
		
		DispatchQueue.main.async(execute: {
			UIApplication.topViewController().present(alert, animated: true, completion: nil)
		})
		
		let delayTime = DispatchTime.now() + Double(Int64(1.0 * Double(NSEC_PER_SEC))) / Double(NSEC_PER_SEC)
		DispatchQueue.main.asyncAfter(deadline: delayTime) {
			alert.dismiss(animated: true, completion: nil)
		}
	}
	
	// MARK: - Storyboard and vc name
	
	/**
	Get base view controller with name and from storyboard

	- parameter storyBoard: Storyboard name
	- parameter vcName:     View contoroller name

	- returns: BaseViewController
	*/
	static func VC(_ storyBoard: String, vcName: String) -> UIViewController {
		
		return UIStoryboard(name: storyBoard, bundle: nil).instantiateViewController(withIdentifier: vcName) 
	}
	
	
    // MARK:- Open url
	
	/**
	Call phone number

	- parameter number: String number
	*/
	static func callNumber(_ number: String) {
		
		if let numberUrl = URL(string: "tel://\(number)") {
			
			if UIApplication.shared.canOpenURL(numberUrl) {
				
                UIApplication.shared.open(numberUrl, options: [:], completionHandler: { (success) in
                    
                })
			}
			else {
				self.alertWithTitle(ERROR, message: NSLocalizedString("Not able to call number", comment: ""))
			}
		}
	}
	
	/**
	Open web page with url string

	- parameter urlString: String url
	*/
	static func openUrl(_ urlString: String) {
		
		if let theUrl = URL(string: urlString) {
			
            let safariVC = SFSafariViewController(url: theUrl)
            UIApplication.topViewController().present(safariVC, animated: true, completion: nil)
		}
	}
	
	/**
	Open location and create direction from current location to destination

	- parameter location: Destionation
	- parameter name:     Name of destination
	*/
	static func openLocationInMaps(_ location: CLLocationCoordinate2D, name: String?) {
		
		let placemark = MKPlacemark(coordinate: location, addressDictionary: nil)
		let mapItem = MKMapItem(placemark: placemark)
		
		mapItem.name = name ?? ""
		
		let launchOptions = [MKLaunchOptionsDirectionsModeKey: MKLaunchOptionsDirectionsModeDriving]
		
		mapItem.openInMaps(launchOptions: launchOptions)
	}
    
    
    // MARK:- XIB helper
    
    /// Register cells from nib
    ///
    /// - parameter table:      table of the cell
    /// - parameter identifier: cell identifier
    class func registerCellFromXib(_ table: UITableView, identifier: String) -> Void {
        let nib: UINib = UINib(nibName: identifier, bundle: nil)
        table.register(nib, forCellReuseIdentifier: identifier)
    }
    
    
    /// Register uiviews from nib
    ///
    /// - parameter name: views identifier
    ///
    /// - returns: uiview
    class func instanceFromNib(name: String) -> UIView {
        return UINib(nibName: name, bundle: nil).instantiate(withOwner: nil, options: nil)[0] as! UIView
    }
    
    
    //MARK:- Localization
    
    /// Return language name for iso code from deserialization
    ///
    /// - Parameter iso: language iso code
    /// - Returns: Language full name
    static func getLanguageName(iso: String?) -> String? {
        
        if iso == nil {
            return nil
        }
        
        for code in Locale.isoLanguageCodes as [String] {
            
            let id = Locale.identifier(fromComponents: [NSLocale.Key.languageCode.rawValue: code])
            let name = (Locale(identifier: "en_UK") as NSLocale).displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Language not found for code: \(code)"
            
            if iso == code {
                
                return name
            
            }
        }
        return nil
    }
    
    /// Return language name for iso code from deserialization
    ///
    /// - Parameter iso: language iso code
    /// - Returns: Language full name
    static func getCountryName(iso: String?) -> String? {
        
        if iso == nil {
            return nil
        }
        
        for code in Locale.isoRegionCodes as [String] {
            
            let id = Locale.identifier(fromComponents: [NSLocale.Key.countryCode.rawValue: code])
            let name = (Locale(identifier: "en_UK") as NSLocale).displayName(forKey: NSLocale.Key.identifier, value: id) ?? "Country not found for code: \(code)"
            
            if iso == code {
                
                return name
            }
        }
        return nil
    }
}


//MARK:- Extensions

extension UIApplication {
	
	/**
	Get top most view controller

	- parameter base: Root view controller

	- returns: Top view controller
	*/
	class func topViewController(_ base: UIViewController? = (UIApplication.shared.delegate as! AppDelegate).window?.rootViewController) -> UIViewController! {
		if let nav = base as? UINavigationController {
			return topViewController(nav.visibleViewController)
		}
		if let tab = base as? UITabBarController {
			if let selected = tab.selectedViewController {
				return topViewController(selected)
			}
		}
		if let presented = base?.presentedViewController {
			return topViewController(presented)
		}
		return base
	}
}

extension Double {
	func format(_ f: String) -> String {
		return String(format: "%\(f)f", self)
	}
}

extension UIViewController {
	
	/**
	Is UIViewController modally presented
	
	- returns: True if UIViewController is presented modally
	*/
	func isModal() -> Bool {
		
		if self.presentingViewController != nil {
			return true
		}
		
		if self.presentingViewController?.presentedViewController == self {
			return true
		}
		
		if self.navigationController?.presentingViewController?.presentedViewController == self.navigationController {
			return true
		}
		
		if self.tabBarController?.presentingViewController is UITabBarController {
			return true
		}
		
		return false
	}
}






