//
//  BaseViewController.swift
//  Quiz4U
//
//  Created by Intellex on 8/15/16.
//  Copyright © 2016 Intellex. All rights reserved.
//

import UIKit
import Foundation

let headerViewIdentifier = "ViewControllerHeaderView"

class BaseViewController: UIViewController, LoadingDataDelegate, TableCellSelectedProtocol, TableCellDeleteDelegate, CollectionCellSelectedProtocol {
	
	let topViewController: UIViewController! = UIApplication.topViewController()

    var footerButton = UIButton()
    
    // Table VC data 
    
    var tableVC: CustomTableViewController?
    
	var loadMore : Bool?
    
    var tableData: [TableSectionData]?{
        
        didSet{
            
            if tableData != nil{
                
                // Remove line before table loads
//                self.tableVC?.tableView.separatorStyle = .none
                
                DispatchQueue.main.async {
                    
                    // Reload table view
                    self.tableVC?.dataArray = self.tableData!
                    self.tableVC?.tableView.reloadData()
//                    self.tableVC?.tableView.separatorStyle = .singleLine
                    
                    if self.tableVC?.tableView(self.tableVC!.tableView, numberOfRowsInSection: 0) == 0 {
                        print("No data")
                    }
                }
            }
        }
    }
    
    var collectionVC : CustomCollectionViewController?
    
    var collectionData: [AnyObject]?{
        
        didSet{
            
            if collectionData != nil{
                
                self.collectionVC?.dataArray = collectionData!
                self.collectionVC?.collectionView?.reloadData()
                
                if (collectionData?.count)! > 0{
                    self.collectionVC?.collectionView?.scrollToItem(at: IndexPath(item: 0, section: 0), at: .top, animated: true)
                }
            }
        }
    }
    
    // MARK:- View cycle
    
	override func viewDidLoad() {
		super.viewDidLoad()
    }

	override func viewDidAppear(_ animated: Bool) {
		super.viewDidAppear(animated)

	}

	override func viewDidDisappear(_ animated: Bool){

		super.viewDidDisappear(animated)
	}
    
    
	// MARK:- Custom navigation bar view delegate
	/**
	CustomNavigationBarDelegate implemented, disimiss or pop VC. Delete EventListener from the ListenerPool
	
	- parameter viewController: ViewController to to be dismissed/popped for which event listeners will be deleted
	*/
	func goBack(_ viewController: UIViewController) {
		
		let isModal = self.isModal()
		
		DispatchQueue.main.async {
			if isModal {
				
				self.dismiss(animated: true, completion: nil)
				
			} else {
				_ = self.navigationController?.popViewController(animated: true)
			}
		}
		
	}
	
	/**
	Open or close side menu VC
	*/
	func sideMenuAction() {
		
		let storyBoard = UIStoryboard.init(name: "SideMenu", bundle: Bundle.main)
		let sideMenuVC = storyBoard.instantiateInitialViewController()
		self.presentNext(sideMenuVC!)
	}
	
	/**
	Back to root view controlelr
	*/
	func goHome() {

       _ = self.navigationController?.popToRootViewController(animated: true)
	}
	
    
    /// Go to main game screen
    func goMainScreen(){
        
        DispatchQueue.main.async {
            
            let homeVC = Util.VC("Main", vcName: "HomeViewController")
            self.navigationController?.pushViewController(homeVC, animated: true)
        }
    }
    
    /// Go to authorization VC
    func goToAuthorization(){
        
        DispatchQueue.main.async {
            
            let authVC = Util.VC("Main", vcName: "AuthViewController")
            self.presentNext(authVC)
        }
    }

	
	// MARK: - View controller transition next
	
	/**
	Push view controller to next VC

	- parameter viewController: <#viewController description#>
	*/
	func goNext(_ viewController: UIViewController) {
		
		self.navigationController?.pushViewController(viewController, animated: true)
	}
	
	/**
	Present next VC

	- parameter viewController: <#viewController description#>
	*/
	func presentNext(_ viewController: UIViewController) {
		
		self.present(viewController, animated: true, completion: nil)
	}
    
    
    //MARK:- Segue
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        
        if segue.destination.isKind(of: CustomTableViewController.self){
            
            self.tableVC = segue.destination as? CustomTableViewController
        }
        else if segue.destination.isKind(of: CustomCollectionViewController.self){
            
            self.collectionVC = segue.destination as? CustomCollectionViewController
        }
    }
	
	// MARK: - Loading data
	
	/**
	Delegate method for loading data for pagination cell
	*/
	func loadingData() {
		
	}
    
    /// Use this method to refresh data on page. Every View controller will define its
    func refreshPage(){
        
        self.view.layoutSubviews()
    }
	
	// MARK:- Table view cell select protocol
	
	/**
	Table cell is selected method

	- parameter data: Table cell data as TableCellData
	*/
	func tableCellSelected(_ data: AnyObject?) {
		
	}
	
	/**
	Table cell is selected for specific path

	- parameter data:      Table cell data
	- parameter indexPath: Selected index path
	*/
	func tableCellSelectedAtIndexPath(_ data: AnyObject?, indexPath: IndexPath) {
		
	}
	
	/**
	Abstract function for delete cell

	- parameter data:      Cell data
	- parameter indexPath: Cell index path
	*/
	func deleteCell(_ data: AnyObject?, indexPath: IndexPath) {
		
	}

	// MARK:- Collection view cell delegate


	/// Abstract func for selected collection cell
	///
	/// - parameter data:      Collection cell data
	/// - parameter indexPath: Collection cell index path
	func collectionCellSelected(_ data: AnyObject?, indexPath: IndexPath) {

	}
	
	// MARK:- Memory management
	
	override func didReceiveMemoryWarning() {
		super.didReceiveMemoryWarning()
		// Dispose of any resources that can be recreated.
	}
	
	}

