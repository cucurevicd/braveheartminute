//
//  AuthViewController.swift
//  Brave Heart Minute
//
//  Created by Vladimir Djokanovic on 8/21/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit
import FirebaseAuth

class AuthViewController: BaseViewController {
    
    var listener : AuthStateDidChangeListenerHandle?

    override func viewDidLoad() {
        super.viewDidLoad()


    }
    
    
    //MARK:- Actions
    
    @IBAction func googleAction(_ sender: UIButton) {
        
        GoogleClient.authorize(from: self, isFirebaseAuth: true) { (success) in
            
            if success{
                
            }
        }
    }
    
    @IBAction func facebookAction(_ sender: UIButton) {
        
        FacebookClient.authorize(from: self, isFirebaseAuth: true) { (success) in
         
            if success{
                
            }
        }
    }
    
    @IBAction func phoneAction(_ sender: UIButton) {
        
        Util.alertWithTextField("Phone authorization", message: "Enter phone number", buttonTitle: "OK", textFieldPlaceholder: "Phone number") { [weak self] (phoneNumber) in
            
            if ValidationUtil.isPhoneValid(phoneNumber){
         
                FirebaseAuthClient.phoneAuth(phoneNumber: phoneNumber, success: { (success) in
                    
                    if !success{
                        Util.alertWithTitle(ERROR, message: "Phone not valid")
                    }
                    else{
                        self?.performSegue(withIdentifier: "verifyPhone", sender: nil)
                    }
                    
                })
            }
            else{
                Util.alertWithTitle(ERROR, message: "Phone not valid")
            }
        }
    }
    
    @IBAction func emailAction(_ sender: UIButton) {
        
        self.performSegue(withIdentifier: "emailAuth", sender: sender)
    }
}
