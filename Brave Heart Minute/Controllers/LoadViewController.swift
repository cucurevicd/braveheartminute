//
//  LoadViewController.swift
//  Brave Heart Minute
//
//  Created by Vladimir Djokanovic on 8/30/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit

class LoadViewController: BaseViewController , UIImagePickerControllerDelegate, UINavigationControllerDelegate{
    
    var imagePicker: UIImagePickerController!

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func downloadAction(_ sender: UIButton) {
        
    }
    
    
    @IBAction func uploadAction(_ sender: UIButton) {
        
        self.imagePicker = UIImagePickerController()
        self.imagePicker.delegate = self
        self.imagePicker.sourceType = .photoLibrary
        self.present(self.imagePicker, animated: true, completion: nil)
    }
    
    // MARK:- Image picker delegate
    
    /**
     UIImagePickerControllerDelegate function implemented
     
     - parameter picker: Image picker
     - parameter info:   Image info
     */
    func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [String: Any]) {
            
        if let image = info[UIImagePickerControllerOriginalImage] as? UIImage {
            
            ServiceRegistry.shared.load.uploadFile(name: "testImage", data: UIImageJPEGRepresentation(image, 1.0)! as NSData, finish: { (finish) in
                print("Finish")
            })
        }
        
        imagePicker.dismiss(animated: true, completion: nil)
    }
    
    // UIImagePickerControllerDelegate function for cancel implemented
    func imagePickerControllerDidCancel(_ picker: UIImagePickerController) {
        imagePicker.dismiss(animated: true, completion: nil)
    }
}
