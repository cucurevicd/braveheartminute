//
//  EmailViewController.swift
//  Brave Heart Minute
//
//  Created by Vladimir Djokanovic on 8/21/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit
import Material

class EmailViewController: BaseViewController {

    @IBOutlet weak var emailTextField: ErrorTextField!
    @IBOutlet weak var passwordTextField: ErrorTextField!
    
    @IBOutlet weak var signInButton: RaisedButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        prepareEmailField()
        preparePasswordField()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func goBackAction(_ sender: UIButton) {
        super.goBack(self)
    }
    
    @IBAction func signInAction(_ sender: Button) {
        
        emailTextField.resignFirstResponder()
        passwordTextField.resignFirstResponder()
        
        if validateData(){
            
            FirebaseAuthClient.auth(email: emailTextField.text!, password: passwordTextField.text!, success: { (success) in
                
                if !success{
                    
                }
                else{
//                    self.dismiss(animated: true, completion: nil)
                }
            })
        }
    }
    
    // Validate data
    func validateData() -> Bool{
        
        let validEmail = ValidationUtil.isValidEmail(emailTextField.text!)
        let validPassword = ValidationUtil.isPasswordLengthMinimum(password: passwordTextField.text!, lenght: 8)
        return validEmail && validPassword
    }
    
    
    fileprivate func prepareEmailField() {
        
        emailTextField.placeholder = "Email"
        emailTextField.detail = "Error, incorrect email"
        emailTextField.isClearIconButtonEnabled = true
        emailTextField.delegate = self
    }
    
    fileprivate func preparePasswordField() {
        
        passwordTextField.placeholder = "Password"
        passwordTextField.detail = "At least 8 characters"
        passwordTextField.detailColor = Color.darkText.secondary
        passwordTextField.clearButtonMode = .whileEditing
        passwordTextField.isVisibilityIconButtonEnabled = true
        passwordTextField.delegate = self
        
        // Setting the visibilityIconButton color.
        passwordTextField.visibilityIconButton?.tintColor = Color.green.base.withAlphaComponent(passwordTextField.isSecureTextEntry ? 0.38 : 0.54) 
    }
}

extension EmailViewController: TextFieldDelegate {
    public func textFieldDidEndEditing(_ textField: UITextField) {
        
        if (textField == emailTextField){
            (textField as? ErrorTextField)?.isErrorRevealed = !ValidationUtil.isValidEmail(emailTextField.text!)
        }
        else if (textField == passwordTextField){
            (textField as? ErrorTextField)?.isErrorRevealed = !ValidationUtil.isPasswordLengthMinimum(password: passwordTextField.text!, lenght: 8)
        }
    }
    
    public func textFieldShouldClear(_ textField: UITextField) -> Bool {
        
        if (textField == emailTextField){
            (textField as? ErrorTextField)?.isErrorRevealed = !ValidationUtil.isValidEmail(emailTextField.text!)
        }
        else if (textField == passwordTextField){
            (textField as? ErrorTextField)?.isErrorRevealed = !ValidationUtil.isPasswordLengthMinimum(password: passwordTextField.text!, lenght: 8)
        }
        
        return true
    }
    
    public func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        
        (textField as? ErrorTextField)?.isErrorRevealed = false
        
        if (textField == passwordTextField){
            (textField as? ErrorTextField)?.isErrorRevealed = !ValidationUtil.isPasswordLengthMinimum(password: string, lenght: 8)
        }
        return true
    }
}
