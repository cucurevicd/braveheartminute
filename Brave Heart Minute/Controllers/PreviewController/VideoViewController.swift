//
//  VideoViewController.swift
//  CustomTransitions
//
//  Created by Dunja Maksimovic on 8/30/17.
//  Copyright © 2017 Appcoda. All rights reserved.
//

import UIKit
import AVKit
import AVFoundation
import AVKit

class VideoViewController: ContentViewController {
    
    var videoVC = AVPlayerViewController()
    var player = AVPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        view.addSubview(videoVC.view)
        videoVC.view.frame = view.bounds
    }

    override func handleContent(_ content: URL) {
        
        player = AVPlayer(url: content)
        videoVC.player = player
        videoVC.player?.play()
    }
}
