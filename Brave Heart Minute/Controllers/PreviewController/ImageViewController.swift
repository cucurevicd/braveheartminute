//
//  ImageViewController.swift
//  CustomTransitions
//
//  Created by Dunja Maksimovic on 8/30/17.
//  Copyright © 2017 Appcoda. All rights reserved.
//

import UIKit

class ImageViewController: ContentViewController {

    @IBOutlet weak var imageView: UIImageView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func handleContent(_ content: URL) {
        
        //
        // Get image from url(content)
        //
        do{
            let data = try Data(contentsOf: content)
            let image = UIImage(data: data)
            imageView.image = image
        }
        catch{
            Util.alertWithTitle(ERROR, message: "No image for this url path")
        }
        
    }
}
