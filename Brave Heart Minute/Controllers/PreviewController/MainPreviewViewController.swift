//
//  MainViewController.swift
//  CustomTransitions
//
//  Created by Dunja Maksimovic on 8/24/17.
//  Copyright © 2017 Appcoda. All rights reserved.
//

import UIKit

class MainPreviewViewController: UIViewController {
    
    // MARK:- Properties
    var notificationModel: NotificationModel!

    //MARK:- View cycle
    
    override func viewDidLoad() {
        super.viewDidLoad()
    
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
       
        if let type = notificationModel.type{
            if let vc = presentViewControllerInContainer(typeName: type){
                addAsChild(vc: vc)
            }
        }
    }
    
    
    func presentViewControllerInContainer(typeName: String) -> ContentViewController?{
        
        var vcName: String?
        
        switch typeName {
        case "Images":
            vcName = "ImageViewController"
            break;
        case "Music":
            vcName = "AudioViewController"
            break;
        case "Videos":
            vcName = "VideoViewController"
            break;
        case "HTML":
            vcName = "WebViewController"
            break;
        default:
            return nil
        }
        
        if vcName != nil{
            return Util.VC("PreviewStoryboard", vcName: vcName!) as? ContentViewController
        }
        
        return nil
    }
    
    // MARK:- Private
    
    private func getContentForNotification(contentFile: @escaping (_ result: FileLoad) -> Void){
        
        if let name = self.notificationModel.fileName, let type = self.notificationModel.type{
        
            let components = name.components(separatedBy: ".")
            if components.count == 2{
             
                ServiceRegistry.shared.load.downloadFile(fileName: components.first!, fileType: type, fileExtension: components.last!, finish: { (downloadFile) in
                    
                    if downloadFile != nil{
                        contentFile(downloadFile!)
                    }
                    else{
                        Util.alertWithTitle(ERROR, message: "No data at file path")
                    }
                })
            }
        }
        
    }
    
    private func getFileID() -> String?{
        
        if let name = self.notificationModel.fileName, let type = self.notificationModel.type{
            
            let components = name.components(separatedBy: ".")
            if components.count == 2{
                return "\(type)/\(components.first!)\(components.last!)"
            }
        }
        return nil
    }

    private func addAsChild(vc: ContentViewController) {
        
        // Clean
        if childViewControllers.count > 0 {
            childViewControllers.first?.removeFromParentViewController()
        }
        
        // Add
        addChildViewController(vc)
        vc.view.frame = CGRect(x: 0, y: 0, width: view.frame.size.width, height: view.frame.size.height - 100)
        view.addSubview(vc.view)
        vc.didMove(toParentViewController: self)
        
        // Handle
        self.getContentForNotification { (file) in
            
            if let path = file.path{
                vc.handleContent(path)
            }
            else{
                Util.alertWithTitle(ERROR, message: "No file path")
            }
        }
    }
}
