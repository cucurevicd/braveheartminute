//
//  WebViewController.swift
//  CustomTransitions
//
//  Created by Dunja Maksimovic on 8/24/17.
//  Copyright © 2017 Appcoda. All rights reserved.
//

import UIKit

class WebViewController: ContentViewController {

    @IBOutlet weak var webView: UIWebView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func handleContent(_ content: URL) {
        
        webView.loadRequest(URLRequest(url: content))
        
    }
}
