//
//  AudioViewController.swift
//  CustomTransitions
//
//  Created by Dunja Maksimovic on 8/24/17.
//  Copyright © 2017 Appcoda. All rights reserved.
//

import UIKit
import AVFoundation

class AudioViewController: ContentViewController, AVAudioPlayerDelegate {

    var audioPlayer = AVAudioPlayer()
//    var player = AVPlayer()
    
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    override func handleContent(_ content: URL) {
    
            // AVPlayer
//            player = AVPlayer(url: content)
//            player.play()
        
            
        //AVAudioPlayer
        do {
            audioPlayer = try AVAudioPlayer(contentsOf: content)
            
            audioPlayer.delegate = self
            
            if audioPlayer.prepareToPlay() {
                print("Preparation success")
                
                if audioPlayer.play() {
                    print("Sound play success")
                } else {
                    print("Sound file could not be played")
                }
            } else {
                print("Preparation failure")
            }
            
        } catch {
            print("Sound file could not be found")
        }
    }
}
