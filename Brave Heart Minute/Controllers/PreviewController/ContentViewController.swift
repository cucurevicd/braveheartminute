//
//  ContentViewController.swift
//  CustomTransitions
//
//  Created by Dunja Maksimovic on 8/30/17.
//  Copyright © 2017 Appcoda. All rights reserved.
//

import UIKit

protocol ContentProtocol {
    func handleContent(_ content: URL)
}

class ContentViewController: UIViewController, ContentProtocol {
    
    var contentType:String?

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    open func handleContent(_ content: URL) {
        print("Error, implement protocol")
    }
}
