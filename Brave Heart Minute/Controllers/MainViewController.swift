//
//  MainViewController.swift
//  Brave Heart Minute
//
//  Created by Vladimir Djokanovic on 8/22/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit

class MainViewController: BaseViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        loadingData()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }

    override func loadingData() {

        let loader = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        loader.center = self.view.center
        self.view.addSubview(loader)
        loader.hidesWhenStopped = true
        loader.startAnimating()
        
        ServiceRegistry.shared.appConfig.sync {
            
            // Reload all data to print new values and images
            DispatchQueue.main.async {
                loader.stopAnimating()
                self.configTableView()
                self.tableVC?.tableView.reloadData()
            }
        }
    }
    
    
    func configTableView(){
        
        var cells = [TableCellData]()

        // Add categories
        cells.append(contentsOf: self.createNotificationCells())
        
        let notifications = TableSectionData(sectionName: nil, data: nil, cellData: cells)
        self.tableData = [notifications]
        
        if cells.count == 0 {
            tableVC?.tableView.separatorColor = UIColor.clear
        }
    }
    
    /// Create categores cell for marathon
    ///
    /// - Returns: Categories cell
    func createNotificationCells() -> [TableCellData]{
        
        var categoriesCells = [TableCellData]()
        
        if let notifications = CoreDataManager.getAllNotifications(){
        
            for notif in notifications{
                
                let categoryCell = TableCellData(name: "NotificationTableCell", data: notif)
                categoriesCells.append(categoryCell)
            }
        }
        
        return categoriesCells
    }

    @IBAction func logoutAction(_ sender: Any) {
        
        FirebaseAuthClient.logout { (success) in
            
            if success{
                LocalNotificationManager.removeAllNotifications()
            }
        }
    }


    //MARK:- Navigation to details View controller
    override func tableCellSelected(_ data: AnyObject?) {
        
        if let notification = data as? NotificationModel{
            
            let nextVC = Util.VC("Main", vcName: "NotificationDetailsViewController") as! NotificationDetailsViewController
            nextVC.notification = notification
            self.goNext(nextVC)
        }
    }
}
