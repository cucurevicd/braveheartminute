//
//  NotificationDetailsViewController.swift
//  Brave Heart Minute
//
//  Created by Vladimir Djokanovic on 8/24/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit

class NotificationDetailsViewController: BaseViewController {
    
    @IBOutlet weak var alertBtn: UIBarButtonItem!
    
    var notification: NotificationModel?{
        
        didSet{
            
            if notification != nil{
                self.configTableView()
            }
        }
    }
    

    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        
        
    }
    
    @IBAction func notificationAlertAction(_ sender: UIBarButtonItem) {
     
        Util.alertWithTitle(ALERT, message: "Notification enable")
    }
    
    
    func configTableView(){
        
        var cells = [TableCellData]()
        
        // Add categories
        cells.append(contentsOf: self.createNotificationCells())
        
        let notifications = TableSectionData(sectionName: nil, data: nil, cellData: cells)
        self.tableData = [notifications]
        
        tableVC?.tableView.separatorStyle = .none
    }

    
    
    /// Create categores cell for notification
    ///
    /// - Returns: Categories cell
    func createNotificationCells() -> [TableCellData]{
        
        var detailCells = [TableCellData]()
       
        // Title
        if let name = notification?.name{

            self.title = name
        }
        
        // Desc
        if let desc = notification?.desc{
            
            let descCell = TableCellData(name: "DescriptionTableCell", data: desc as AnyObject)
            detailCells.append(descCell)
        }
        
        // LunchTime
        if let time = notification?.triggerDate{
            
            let lunchTimeCell = TableCellData(name: "LunchTimeTableCell", data: time as AnyObject)
            detailCells.append(lunchTimeCell)
        }

        // Watched
        if let watched = notification?.watched{
            
            if watched{
                
                if let watchedTime = notification?.watchedDate{
                 
                    let watchedTimeCell = TableCellData(name: "LunchTimeTableCell", data: watchedTime as AnyObject)
                    detailCells.append(watchedTimeCell)
                }
            }
        }
        
        // Preloader
        let preloaderCell = TableCellData(name: "PreloadTableCell", data: notification as AnyObject)
        detailCells.append(preloaderCell)
        
        
        // Watched
        let watchActionCell = TableCellData(name: "WorkTableCell", data: notification as AnyObject)
        detailCells.append(watchActionCell)
        
        return detailCells
    }
}
