//
//  VerifyPhoneViewController.swift
//  AuthorizationApp
//
//  Created by Vladimir Djokanovic on 8/14/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit
import FirebaseAuth

class VerifyPhoneViewController: BaseViewController {

    @IBOutlet weak var conformationCodeTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    @IBAction func buttonAction(_ sender: UIButton) {
    
        if ValidationUtil.isInputFiledEmpty(conformationCodeTextField.text!){
           
            Util.alertWithTitle(ERROR, message: "Enter conformation code")
            return
        }
        
        FirebaseAuthClient.verifyPhone(verifyCode: conformationCodeTextField.text!, success: { (success) in
            
            if success{
                
                // TODO: Go to app
            }
        })
    }
    
    @IBAction func goBackAction(_ sender: UIButton) {
        super.goBack(self)
    }
}
