//
//  LocalNotificationCategory.swift
//  Brave Heart Minute
//
//  Created by Vladimir Djokanovic on 8/24/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI

enum LocalNotificationType {
    case Snooze
    case Default
    
    var description : String {
        switch self {
        // Use Internationalization, as appropriate.
        case .Snooze: return "Snooze";
        case .Default: return "Default";
        }
    }
    
    static let allValues = [.Snooze, Default]
}

class LocalNotificationCategory: NSObject {
    
    

    static func createSnoozeCategory() -> UNNotificationCategory{
        
        // Create the custom actions for the TIMER_EXPIRED category.
        let snoozeAction = UNNotificationAction(identifier: LocalNotificationActionType.Snooze.description,
                                                title: "Snooze",
                                                options: [])
        
        
        return UNNotificationCategory(identifier: LocalNotificationType.Snooze.description,
                                                     actions: [snoozeAction],
                                                     intentIdentifiers: [],
                                                     options: UNNotificationCategoryOptions(rawValue: 0))
    }
}
