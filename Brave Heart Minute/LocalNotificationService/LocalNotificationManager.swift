//
//  LocalNotificationManager.swift
//  Brave Heart Minute
//
//  Created by Vladimir Djokanovic on 8/24/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit
import UserNotifications
import UserNotificationsUI

class LocalNotificationManager: NSObject {
    
    
    /// Check is notifcation active
    ///
    /// - Parameters:
    ///   - identifier: Identifier of notificaiton
    ///   - active: Notification turn on/off
    static func notificationActive(identifier: String, active: @escaping (_ active: Bool) -> Void){
        
        UNUserNotificationCenter.current().getPendingNotificationRequests { (requests) in
            
            for request in requests{
                
                if request.identifier == identifier{
                    active(true)
                    return
                }
            }
            active(false)
        }
    }
    
    /// Add notification to local center
    ///
    /// - Parameters:
    ///   - appNotification: Notificaiton model
    ///   - success: Success
    static func addNotificaiton(appNotification: NotificationModel, success: @escaping SuccessHandler){
        
        if let date = appNotification.triggerDate{
            
            var components = date.components(separatedBy: ":")
            let hour = Int(components[0])
            let minutes = Int(components[1])
            var dateComponents = DateComponents()
            dateComponents.hour = hour
            dateComponents.minute = minutes
            
            let trigger = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
            
            let request = UNNotificationRequest(identifier: appNotification.id!, content: createUNNotification(notification: appNotification), trigger: trigger)
            
            UNUserNotificationCenter.current().add(request, withCompletionHandler: { (error) in
                
                success(error == nil)
            })
        }
    }
    
    /// Remove specific notification from center
    ///
    /// - Parameters:
    ///   - appNotification: Notificaiton to be removed
    static func removeNotificaiton(appNotification: NotificationModel){
        
        if let id = appNotification.id{
            
            UNUserNotificationCenter.current().removePendingNotificationRequests(withIdentifiers: [id])
        }
    }
    
    // MARK:- Private
    
    private static func createUNNotification(notification: NotificationModel) -> UNMutableNotificationContent {
        
        let content = UNMutableNotificationContent()
        
        if let name = notification.name{
            
            content.title = name
        }
        
        if let desc = notification.desc{
            
            content.body = desc
        }
        
        content.sound = UNNotificationSound.default()
        content.categoryIdentifier = LocalNotificationType.Snooze.description
        
        return content
        
    }
    
    
    /// On logut remove all notifications
    static func removeAllNotifications(){
        
        UNUserNotificationCenter.current().removeAllPendingNotificationRequests()
    }
    
}
