//
//  LocalNotificationAction.swift
//  Brave Heart Minute
//
//  Created by Vladimir Djokanovic on 8/24/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit
import UserNotifications

enum LocalNotificationActionType {
    case Snooze
    case Default
    
    var description : String {
        switch self {
        // Use Internationalization, as appropriate.
        case .Snooze: return "Snooze";
        case .Default: return "Default";
        }
    }
}
class LocalNotificationAction: NSObject {

    static func getAction(identifier: String, request: UNNotificationRequest){
        
        switch identifier {
        case LocalNotificationActionType.Snooze.description:
            snoozeAction(request: request)
            break
        case LocalNotificationActionType.Default.description:
            break
            
        default:
            break
        }
    }
    
    static func snoozeAction(request: UNNotificationRequest){
        
        if let trigger = request.trigger as? UNCalendarNotificationTrigger{
            
            var dateComponents = DateComponents()
            
            if trigger.dateComponents.minute! < 60 {
                let minute = trigger.dateComponents.minute! + 1
             
                dateComponents.minute = minute
                dateComponents.hour = trigger.dateComponents.hour
            }
            else{
                let minute = trigger.dateComponents.minute!
                let hour = dateComponents.hour! + 1
                
                dateComponents.minute = minute
                dateComponents.hour = hour
            }
            
            let triggerNew = UNCalendarNotificationTrigger(dateMatching: dateComponents, repeats: true)
            let requestNew = UNNotificationRequest(identifier: request.identifier, content: request.content, trigger: triggerNew)
            UNUserNotificationCenter.current().add(requestNew, withCompletionHandler: { (error) in
                print(error as Any)
            })
        }
    }
    
}
