//
//  NotificationModel+CoreDataProperties.swift
//  
//
//  Created by Vladimir Djokanovic on 9/1/17.
//
//

import Foundation
import CoreData


extension NotificationModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<NotificationModel> {
        return NSFetchRequest<NotificationModel>(entityName: "NotificationModel")
    }

    @NSManaged public var dataUrl: String?
    @NSManaged public var desc: String?
    @NSManaged public var id: String?
    @NSManaged public var name: String?
    @NSManaged public var status: String?
    @NSManaged public var triggerDate: String?
    @NSManaged public var type: String?
    @NSManaged public var watched: Bool
    @NSManaged public var watchedDate: NSDate?
    @NSManaged public var fileName: String?
    @NSManaged public var category: CategoryModel?

}
