//
//  CategoryModel+CoreDataProperties.swift
//  
//
//  Created by Vladimir Djokanovic on 8/29/17.
//
//

import Foundation
import CoreData


extension CategoryModel {

    @nonobjc public class func fetchRequest() -> NSFetchRequest<CategoryModel> {
        return NSFetchRequest<CategoryModel>(entityName: "CategoryModel")
    }

    @NSManaged public var color: String?
    @NSManaged public var icon: String?
    @NSManaged public var id: String?
    @NSManaged public var name: String?
    @NSManaged public var notifications: NSSet?

}

// MARK: Generated accessors for notifications
extension CategoryModel {

    @objc(addNotificationsObject:)
    @NSManaged public func addToNotifications(_ value: NotificationModel)

    @objc(removeNotificationsObject:)
    @NSManaged public func removeFromNotifications(_ value: NotificationModel)

    @objc(addNotifications:)
    @NSManaged public func addToNotifications(_ values: NSSet)

    @objc(removeNotifications:)
    @NSManaged public func removeFromNotifications(_ values: NSSet)

}
