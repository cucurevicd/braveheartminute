//
//  CoreDataManager.swift
//  Brave Heart Minute
//
//  Created by Vladimir Djokanovic on 8/28/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit
import Sync

class CoreDataManager: NSObject {
    
    static let context = (UIApplication.shared.delegate as! AppDelegate).managedObjectContext
    static let dataStack =  DataStack(modelName: "CoreModel")
    
    static func storeCategories(categories: [Category], finish: @escaping FinishHandler){
        
        var notifyJsons = [[String: Any]]()
        for notif in categories {
            notifyJsons.append(notif.toJSON())
        }
        
        self.saveNotification(notifJSONs: notifyJsons, entityName: "CategoryModel", finish: { (finished) in
            
            finish()
        })
    }
    
    static func storeSyncData(notifications: [AppNotification], finish: @escaping FinishHandler){
        
        var notifyJsons = [[String: Any]]()
        for notif in notifications {
            notifyJsons.append(notif.toJSON())
        }
        
        self.saveNotification(notifJSONs: notifyJsons, entityName: "NotificationModel", finish: { (finished) in
            
            finish()
        })
    }
    
    static func getNotifications(id: String) -> NotificationModel?{
        
        var notifications : NotificationModel?
        do {
            notifications = try dataStack.fetch(id, inEntityNamed: "NotificationModel")
        }catch {
            print("Error fetching data from CoreData")
        }
        return notifications
    }
    
    static func getAllNotifications() -> [NotificationModel]?{
        
        
        let notificationFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "NotificationModel")
        do {
            return try dataStack.mainContext.fetch(notificationFetch) as? [NotificationModel]
        } catch {
            fatalError("Failed to fetch notifications: \(error)")
        }
        return nil
    }
    
    // MARK:- Private
    
    private static func saveNotification(notifJSONs: [[String: Any]], entityName: String, finish: @escaping FinishHandler){
        
        Sync.changes(notifJSONs, inEntityNamed: entityName, dataStack: dataStack ,operations: [.all]) { (error) in
            finish()
            if error != nil{
                print(error as Any)
            }
        }
    }
    
    

}
