//
//  PreloadTableCell.swift
//  Brave Heart Minute
//
//  Created by Vladimir Djokanovic on 8/24/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit

class PreloadTableCell: BaseTableViewCell {

    
    @IBOutlet weak var preloadWebView: UIWebView!
    
    override func configCellWithdata(_ data: AnyObject?, context: BaseViewController?, indexPath: IndexPath?, lastCell: Bool, isCellSelected: Bool?) {
        super.configCellWithdata(data, context: context, indexPath: indexPath, lastCell: lastCell, isCellSelected: isCellSelected)
        
        if let data = data as? NotificationModel{
            
            
            if let url = data.dataUrl{
                loadRequest(url: url)
            }
        }
    }
    
    func loadRequest(url: String){
        
        let mainUrl = URL(string: url)
        let request = URLRequest(url: mainUrl!)
        preloadWebView.loadRequest(request)
    }

}
