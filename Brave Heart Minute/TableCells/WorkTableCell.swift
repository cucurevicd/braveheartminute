//
//  WorkTableCell.swift
//  Brave Heart Minute
//
//  Created by Vladimir Djokanovic on 8/24/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit

class WorkTableCell: BaseTableViewCell {

    //MARK:- Properties
    @IBOutlet weak var workButton: UIButton!
    
    var notificaiton: NotificationModel?{
        
        didSet{
           
            self.configUI()
        }
    }
    
    //MARK:- Config
    
    override func configCellWithdata(_ data: AnyObject?, context: BaseViewController?, indexPath: IndexPath?, lastCell: Bool, isCellSelected: Bool?) {
        super.configCellWithdata(data, context: context, indexPath: indexPath, lastCell: lastCell, isCellSelected: isCellSelected)
        
        if let notification = data as? NotificationModel{
            
            self.notificaiton = notification;
        }
    }
    
    func configUI(){
        
        if let watched = notificaiton?.watched{
            
            workButton.isEnabled = !watched
        }
    }
    
    @IBAction func workAction(_ sender: UIButton) {
     
        let nextVc = Util.VC("PreviewStoryboard", vcName: "MainPreviewViewController") as? MainPreviewViewController
        nextVc?.notificationModel = notificaiton
        (UIApplication.topViewController() as? BaseViewController)?.goNext(nextVc!)
    }

}
