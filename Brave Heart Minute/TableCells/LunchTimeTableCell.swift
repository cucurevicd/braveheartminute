//
//  LunchTimeTableCell.swift
//  Brave Heart Minute
//
//  Created by Vladimir Djokanovic on 8/24/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit

class LunchTimeTableCell: BaseTableViewCell {

    //MARK:- Properties
    
    @IBOutlet weak var timeLabel: UILabel!
    
    
    //MARK:- Config
    
    override func configCellWithdata(_ data: AnyObject?, context: BaseViewController?, indexPath: IndexPath?, lastCell: Bool, isCellSelected: Bool?) {
        super.configCellWithdata(data, context: context, indexPath: indexPath, lastCell: lastCell, isCellSelected: isCellSelected)
        
        if let time = data as? String{
            
            self.timeLabel.text = time
        }
    }

}
