//
//  DescriptionTableCell.swift
//  Brave Heart Minute
//
//  Created by Vladimir Djokanovic on 8/24/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit

class DescriptionTableCell: BaseTableViewCell {

    //MARK:- Properties
    
    @IBOutlet weak var textDescription: UITextView!
    
    
    override func configCellWithdata(_ data: AnyObject?, context: BaseViewController?, indexPath: IndexPath?, lastCell: Bool, isCellSelected: Bool?) {
        super.configCellWithdata(data, context: context, indexPath: indexPath, lastCell: lastCell, isCellSelected: isCellSelected)
        
        if let desc = data as? String{
            
            self.textDescription.text = desc
        }
    }

}
