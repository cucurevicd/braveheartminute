//
//  NotificationTableCell.swift
//  Brave Heart Minute
//
//  Created by Vladimir Djokanovic on 8/23/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit

class NotificationTableCell: BaseTableViewCell {
    
    //MARK:- Params
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    @IBOutlet weak var notificationNotifier: UISwitch!
    @IBOutlet weak var dateLastWatched: UILabel!
    
    var notificaiton: NotificationModel?{
        
        didSet{
        
            self.configUI()
        }
    }
    

    // MARK:- Config
    override func configCellWithdata(_ data: AnyObject?, context: BaseViewController?, indexPath: IndexPath?, lastCell: Bool, isCellSelected: Bool?) {
        super.configCellWithdata(data, context: context, indexPath: indexPath, lastCell: lastCell, isCellSelected: isCellSelected)
        
        if let not = data as? NotificationModel{
            
            self.notificaiton =  not
            self.notificationNotifier.isEnabled = true
        }
        else{
            self.notificationNotifier.isEnabled = false
        }
    }
    
    
    func configUI(){
        
        if let name = notificaiton?.name{
            
            self.nameLabel.text = name
        }
        
        if let status = notificaiton?.category?.icon{
            
            self.statusLabel.text = status
        }
        
        if let lunchTime = notificaiton?.triggerDate{
            
            self.dateLastWatched.text = lunchTime
        }
        
        if let wathced = notificaiton?.watched{
            
            if wathced {
                
                if let dateWatched = notificaiton?.category?.icon{
                 
                    self.dateLastWatched.text = dateWatched
                }
                
                self.notificationNotifier.isEnabled = false
            }
            else{
                self.notificationNotifier.isEnabled = true
            }
        }
        
        if let id = notificaiton?.id{
            LocalNotificationManager.notificationActive(identifier: id, active: { (isActive) in
                
                DispatchQueue.main.async {
                    self.notificationNotifier.isOn = isActive
                }
            })
        }
        
        // Background color
        if let categoryColor = notificaiton?.category?.color{
            
            self.backgroundColor = Util.hexStringToUIColor(hex: categoryColor)
        }
    }

    @IBAction func addLocalNotification(_ sender: UISwitch) {
        
        if sender.isOn {
            
            LocalNotificationManager.addNotificaiton(appNotification: self.notificaiton!, success: { (success) in
                
                DispatchQueue.main.async {
                    sender.isOn = success
                }
            })
            
        }
        else{
            
            LocalNotificationManager.removeNotificaiton(appNotification: self.notificaiton!)
        }
 
    }
}
