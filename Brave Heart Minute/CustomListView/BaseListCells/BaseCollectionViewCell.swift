//
//  BaseCollectionViewCell.swift
//  EKapija
//
//  Created by Intellex on 12/15/15.
//  Copyright © 2015 Intellex. All rights reserved.
//

import UIKit

/**
*  Protocol for selected collection cell
*/
protocol CollectionCellSelectedProtocol{
	func collectionCellSelected(_ data : AnyObject?, indexPath: IndexPath)
}

class BaseCollectionViewCell: UICollectionViewCell {

	//MARK:- Properties


	/// Main View
	@IBOutlet weak var mainView: UIView!

	/// Main data for cell
	var mainData : AnyObject?

	/// Context where collection view is implemented
	var context : BaseViewController?

	/// Index path of cell
	var indexPath : IndexPath!

	/// Collection cell selecte delegate
	var collectionCellSelectedDelegate : CollectionCellSelectedProtocol?

	/// Cell selection
	override var isSelected : Bool{
		didSet {
			if isSelected {self.cellIsSelected()}
		}
	}

	//MARK:- Methods

	override func awakeFromNib() {
		super.awakeFromNib()
		// Initialization code
	}

	/**
	Apstract function for all cells. Cells use data which will be parsed in particullar cell.

	- parameter data:      Data to configure cell
	- parameter context:   View controller which where cell is presenting
	- parameter indexPath: Index path of cell
	*/
	func configCellWithdata(_ data: AnyObject?, context: BaseViewController?, indexPath : IndexPath){

		/**
		*  Set all data to cell instance
		*/
		self.mainData = data
		self.context = context
		self.indexPath = indexPath
	}

	/**
	Apstract function to be called when cell is selected. Also delegat method implemented
	*/
	func cellIsSelected(){

		/**
		*  If we need delegate for calling method for selecting cell outside the cell class
		*/
		if (collectionCellSelectedDelegate != nil) {
			self.collectionCellSelectedDelegate?.collectionCellSelected(self.mainData, indexPath: self.indexPath)
		}
	}
    
	/**
	Apstract function called for pagination
	*/
	func paginationData(){
		
	}

}
