//
//  LoadingTableCell.swift
//  BusinessSeats
//
//  Created by Intellex on 3/15/16.
//  Copyright © 2016 Intellex. All rights reserved.
//

import UIKit

/**
*  Loading protocol
*/
protocol LoadingDataDelegate{
	func loadingData()
}

class LoadingTableCell: BaseTableViewCell {

	/// Loading delegate
	var delegate : LoadingDataDelegate?

	/// Activity indicator for cell
	@IBOutlet var activityIndicator: UIActivityIndicatorView!

	/**
	Config loading cell in table view

	- parameter data:      nil
	- parameter context:   context
	- parameter indexPath: index path for last cell in section
	- parameter lastCell:  nil
	*/
	override func configCellWithdata(_ data: AnyObject?, context: BaseViewController?, indexPath: IndexPath?, lastCell: Bool, isCellSelected: Bool?) {
		super.configCellWithdata(data, context: context, indexPath: indexPath, lastCell: lastCell, isCellSelected: isCellSelected)

		self.activityIndicator.startAnimating()

		delegate?.loadingData()
	}
}
