//
//  CategoryService.swift
//  Brave Heart Minute
//
//  Created by Vladimir Djokanovic on 8/22/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit
import FirebaseDatabase

class CategoryService: BackendService {
    
    
    func getAllCategories(result: @escaping (_ allCategories: [Category]?) -> Void) {
        
        let operation = BOAllCategories()
        operation.onSuccess = { (data, status) in
            
            var allCategories = [Category]()
            print(data as Any)
            
            var categories = [Any]()
            
            if let data = data as? DataSnapshot{
                let obj = data.value
                print(obj)
                categories = data.value as! [Any]
            }
            
            for cat in categories{
                
                let category = Category(value: cat)
                allCategories.append(category)
            }
            
            result(allCategories)
        }
        
        operation.onFailure = { (error, statusCode) in
        
            print(error?.localizedDescription as Any)
            result(nil)
        }
        
        self.queue?.addOperation(operation: operation)
    }
    
    func getCategory(categoryId: String, result: @escaping(_ category: Category?) -> Void){
        
        let operation = BOGetCategory(categoryId: categoryId)
        operation.onSuccess = { (data, statusCode) in
            
            print(data as Any)
            
            let category = Category(value: data)
            result(category)
        }
        
        operation.onFailure = { (error, statusCode) in
            
            print(error?.localizedDescription as Any)
            result(nil)
        }
        
        self.queue?.addOperation(operation: operation)
    }
}
