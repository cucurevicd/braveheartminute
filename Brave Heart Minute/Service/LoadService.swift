//
//  LoadService.swift
//  Brave Heart Minute
//
//  Created by Vladimir Djokanovic on 8/30/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit

class LoadService: BackendService {

    var loadController: FileLoadController?
    
    func downloadFile(fileName: String, fileType: String, fileExtension: String, finish: @escaping (_ file : FileLoad?) -> Void){
        
        let fileId = "\(fileType)/\(fileName).\(fileExtension)"
        
        self.loadController = FileLoadController(fileId: fileId)
        let file = FileLoad.getFile(fileId: fileId, data: NSData())
        
        let operation = BODownloadFile(fileId: fileId, path: fileType, name: fileName, fileExtension: fileExtension)
        
        operation.onSuccess = { (data, statusCode) in
         
            print(file.progress)
            print(statusCode)
            
            if data != nil{
                print(data!)
            }
            
            finish(file)
        }
        
        operation.onFailure = { (error, statusCode) in
        
            print(error?.localizedDescription as Any)
            finish(nil)
        }
        
        self.queue?.addOperation(operation: operation)
        
        loadController?.subscribeForFileUpload { (file) in
            print(file.progress)
        }
    }
    
    func uploadFile(name:String, data: NSData, finish : @escaping FinishHandler){
        
        let fExtens = "jpg"
        let path = "Images"
        
        let fileId = "uploadImage1"
        
        self.loadController = FileLoadController(fileId: fileId)
        let file = FileLoad.getFile(fileId: fileId, data: data)
        
        let operation = BOUploadFile(fileId: fileId, path: path, name: name, fileExtension: fExtens)
        
        operation.onSuccess = { (data, statusCode) in
            
            print(file.progress)
            print(statusCode)
            
            if data != nil{
                print(data!)
            }
            
            finish()
        }
        
        operation.onFailure = { (error, statusCode) in
            
            print(error?.localizedDescription as Any)
            finish()
        }
        
        self.queue?.addOperation(operation: operation)
        
        loadController?.subscribeForFileUpload { (file) in
            print(file.progress)
        }

    }
}
