//
//  AppConfigService.swift
//  Brave Heart Minute
//
//  Created by Vladimir Djokanovic on 8/23/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit

class AppConfigService: BackendService {

    var categories: [Category]?
    var notifications: [AppNotification]?
    
    func sync(success: @escaping FinishHandler){
        
        // Get categories
        let finishOperation = BlockOperation {
            
            print("This is finished")
            success()
        }
        
        let operation = BOAllCategories()
        operation.onSuccess = { (data, status) in
            
            print(data as Any)
            self.categories = ModelParser.parseArray(data: data!,type: Category.self)
        }
        operation.onFailure = { (error, statusCode) in
            
            print(error?.localizedDescription as Any)
            operation.cancel()
        }
        
        let operationStoreCategories = BlockOperation {
            
            if self.categories != nil{
                CoreDataManager.storeCategories(categories: self.categories!, finish: { (finish) in })
            }
        }
        
        // Get notifications
        let operation2 = BOAllNotifications()
        operation2.onSuccess = { (data, statusCode) in
            
            print(data as Any)
            self.notifications = ModelParser.parseArray(data: data!, type: AppNotification.self)
        }
        
        operation2.onFailure = { (error, statusCode) in
            
            print(error?.localizedDescription as Any)
            operation2.cancel()
        }
        
        let operationStoreNotifications = BlockOperation{
            
            // Store all notifications
            if self.notifications != nil && self.categories != nil{
                CoreDataManager.storeSyncData(notifications: self.notifications!, finish: { (finish) in })
            }
        }
        
        operationStoreCategories.addDependency(operation)
        operation2.addDependency(operationStoreCategories)
        operationStoreNotifications.addDependency(operation2)
        finishOperation.addDependency(operationStoreNotifications)
        
        self.queue?.addOperations(operations: [operation, operation2, operationStoreNotifications, operationStoreCategories, finishOperation])
    }
}
