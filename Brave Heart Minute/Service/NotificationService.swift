//
//  NotificationService.swift
//  Brave Heart Minute
//
//  Created by Vladimir Djokanovic on 8/22/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit

class NotificationService: BackendService {

 
    func getAllNotificaitons(result : @escaping(_ notificaitons : [AppNotification]?) -> Void){
        
        let operation = BOAllNotifications()
        operation.onSuccess = { (data, statusCode) in
        
            print(data as Any)
            
            var allNotifications = [AppNotification]()
            print(data as Any)
            if let notifics = data as? [Any]{
                
                for not in notifics{
                    
                    let notification = AppNotification(value: not)
                    allNotifications.append(notification)
                }
            }
            
            result(allNotifications)
        }
        
        operation.onFailure = { (error, statusCode) in
            
            print(error?.localizedDescription as Any)
            result(nil)
        }
        
        self.queue?.addOperation(operation: operation)
    }
    
    func updateNotification(notif: AppNotification, success: @escaping SuccessHandler){
        
        let operation = BOUpdateNotification(notif: notif)
        operation.onSuccess = {(data, statusCode) in
        
            print(data as Any)
            success(statusCode == 200)
        }
        
        operation.onFailure = { (error, statusCode) in
            
            print(error?.localizedDescription as Any)
            success(false)
        }
        
        self.queue?.addOperation(operation: operation)
    }
}
