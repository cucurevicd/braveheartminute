//
//  ServiceRegistry.swift
//  Service
//
//  Created by Vladimir Djokanovic on 8/15/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit

class ServiceRegistry: NSObject {
    
    static let shared = ServiceRegistry()
    
    // MARK:- List of Services
    let appConfig = AppConfigService()
    let category = CategoryService()
    let notification = NotificationService()
    let load = LoadService()
    
}
