//
//  Constants.swift
//  Service
//
//  Created by Vladimir Djokanovic on 8/15/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import Foundation

#if DEBUG
    let SERVER_URL = "https://www.fotomarathon.at/api/v1/"
#else
    let SERVER_URL = "http://prod.server.com/api/"
#endif
