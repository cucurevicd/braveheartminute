//
//  BRAllNotifications.swift
//  Brave Heart Minute
//
//  Created by Vladimir Djokanovic on 8/22/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit

class BRAllNotifications: NSObject, BackendRequest {

    //MARK:- Init
    override init() {
        super.init()
        //TODO
    }
    
    //MARK:- Backend request
    
    func endpoint() -> String {
        return "Notifications"
    }
    
    func method() -> HttpMethod {
        return .get
    }
    
    func paramteres() -> Dictionary<String, Any>? {
        return nil
    }
    
    func headers() -> Dictionary<String, String>? {
        return nil
    }
    
    func firebaseObserver() -> Bool?{
        return true
    }
    
    func requestType() -> RequestType? {
        return .rest
    }
}
