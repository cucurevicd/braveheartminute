//
//  BRUpdateNotificaiton.swift
//  Brave Heart Minute
//
//  Created by Vladimir Djokanovic on 8/22/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit

class BRUpdateNotificaiton: NSObject, BackendRequest {

    var notif : AppNotification?
    
    //MARK:- Init
    init(notification: AppNotification) {
        super.init()
        
        self.notif = notification
    }
    
    //MARK:- Backend request
    
    func endpoint() -> String {
        return "Notifications"
    }
    
    func method() -> HttpMethod {
        return .post
    }
    
    func paramteres() -> Dictionary<String, Any>? {
        return [(notif!.key)! : self.notif?.toJSON() ?? NSNull()]
    }
    
    func headers() -> Dictionary<String, String>? {
        return nil
    }
    
    func firebaseObserver() -> Bool?{
        return nil
    }
    
    func requestType() -> RequestType? {
        return .rest
    }
}
