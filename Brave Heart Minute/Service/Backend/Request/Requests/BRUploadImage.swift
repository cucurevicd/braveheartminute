//
//  BRUploadImage.swift
//  Brave Heart Minute
//
//  Created by Vladimir Djokanovic on 8/30/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit

class BRUploadImage: NSObject, BackendRequest {

    var fileId: String!
    var path: String!
    var name: String!
    var fileExtension: String!
    var fileData: NSData!
    
    //MARK:- Init
    init(fileId: String, path: String!, name: String, fileExtension: String) {
        super.init()
        
        self.fileId = fileId
        self.path = path
        self.name = name
        self.fileExtension = fileExtension
    }
    
    //MARK:- Backend request
    
    func endpoint() -> String {
        return ""
    }
    
    func method() -> HttpMethod {
        return .post
    }
    
    func paramteres() -> Dictionary<String, Any>? {
        
        return [BRFileIdConst : fileId, BRFilePathConst : path, BRFileNameConst : name, BRFileExtensionConst : fileExtension]
    }
    
    func headers() -> Dictionary<String, String>? {
        return nil
    }
    
    func requestType() -> RequestType? {
        return .upload
    }
    
    func firebaseObserver() -> Bool?{
        return nil
    }
}
