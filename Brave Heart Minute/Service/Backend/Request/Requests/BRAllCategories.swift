//
//  BRAllCategories.swift
//  Brave Heart Minute
//
//  Created by Vladimir Djokanovic on 8/22/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit


class BRAllCategories: NSObject, BackendRequest {


    //MARK:- Init
    override init() {
        super.init()
        //TODO
    }
    
    //MARK:- Backend request
    
    func endpoint() -> String {
        return "Categories"
    }
    
    func method() -> HttpMethod {
        return .get
    }
    
    func paramteres() -> Dictionary<String, Any>? {
        return nil
    }
    
    func headers() -> Dictionary<String, String>? {
        return nil
    }
    
    func requestType() -> RequestType? {
        return .rest
    }
    
    func firebaseObserver() -> Bool?{
        return true
    }
}
