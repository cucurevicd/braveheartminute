//
//  BRGetCategory.swift
//  Brave Heart Minute
//
//  Created by Vladimir Djokanovic on 8/22/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit

class BRGetCategory: NSObject, BackendRequest {
    
    var categoryId: String?
    
    //MARK:- Init
    init(catId: String) {
        super.init()
        
        categoryId = catId
    }
    
    //MARK:- Backend request
    
    func endpoint() -> String {
        return "Categories/\(categoryId!)"
    }
    
    func method() -> HttpMethod {
        return .get
    }
    
    func paramteres() -> Dictionary<String, Any>? {
        return nil
    }
    
    func headers() -> Dictionary<String, String>? {
        return nil
    }
    
    func firebaseObserver() -> Bool?{
        return true
    }
    
    func requestType() -> RequestType? {
        return .rest
    }
}
