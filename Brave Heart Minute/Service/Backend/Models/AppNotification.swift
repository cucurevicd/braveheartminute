//
//  AppNotification.swift
//  Brave Heart Minute
//
//  Created by Vladimir Djokanovic on 8/22/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit
import ObjectMapper

class AppNotification: FirBModel {

    var category_id: String?
    var name: String?
    var dataUrl: String?
    var status: String?
    var type: String?
    var watchDate: String?
    var watched : NSNumber?
    var desc: String?
    var lunchTime: String?
    var category: Category?
    var fileName: String?
    
    // Mappable
    override func mapping(map: Map) {
        
        key <- map["id"]
        name <- map["name"]
        dataUrl <- map["dataUrl"]
        status <- map["status"]
        desc <- map["desc"]
        watched <- map["watched"]
        category_id <- map["category_id"]
        lunchTime <- map["triggerDate"]
        watchDate <- map["watchedDate"]
        type <- map["type"]
        fileName <- map["fileName"]
    }
}
