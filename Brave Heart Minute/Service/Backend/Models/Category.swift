//
//  Category.swift
//  Brave Heart Minute
//
//  Created by Vladimir Djokanovic on 8/22/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit
import FirebaseDatabase
import ObjectMapper

class Category: FirBModel {
    
    // MARK:- Params
    var name: String?
    var color: UIColor?
    var icon: String?
    
    // Mappable
    override func mapping(map: Map) {
        
        name <- map["name"]
        key <- map["id"]
        color <- map["color"]
        icon <- map["icon"]
    }
}
