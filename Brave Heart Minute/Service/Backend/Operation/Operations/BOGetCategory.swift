//
//  BOGetCategory.swift
//  Brave Heart Minute
//
//  Created by Vladimir Djokanovic on 8/22/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit

class BOGetCategory: BackendOperation {
    
    
    init(categoryId: String) {
        super.init()
        
        self.request = BRGetCategory(catId: categoryId)
    }

}
