//
//  BOUpdateNotification.swift
//  Brave Heart Minute
//
//  Created by Vladimir Djokanovic on 8/22/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit

class BOUpdateNotification: BackendOperation {

    
    init(notif: AppNotification) {
        super.init()
        
        self.request = BRUpdateNotificaiton(notification: notif)
    }
}
