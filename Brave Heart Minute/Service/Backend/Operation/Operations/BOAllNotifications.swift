//
//  BOAllNotifications.swift
//  Brave Heart Minute
//
//  Created by Vladimir Djokanovic on 8/22/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit

class BOAllNotifications: BackendOperation {

    
    override init() {
        super.init()
        
        self.request = BRAllNotifications()
    }
}
