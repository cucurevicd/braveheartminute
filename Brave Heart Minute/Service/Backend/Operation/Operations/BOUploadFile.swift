//
//  BOUploadFile.swift
//  Brave Heart Minute
//
//  Created by Vladimir Djokanovic on 8/30/17.
//  Copyright © 2017 Intellex. All rights reserved.
//

import UIKit

class BOUploadFile: BackendOperation {

    init(fileId: String, path: String!, name: String, fileExtension: String) {
        super.init()
        
        self.request = BRUploadImage(fileId: fileId, path: path, name: name, fileExtension: fileExtension)
    }
}
